/**
 * Created by isa on 18/3/15.
 */
angular
    .module('uzchat',['firebase'])
    .controller('mainController', mainController);

function mainController($scope, Firebase, $firebaseArray){
    var repo = new Firebase('https://userzoomchat.firebaseio.com/');
    $scope.username = '';
    $scope.messages = $firebaseArray(repo);

    $scope.addMessage = addMessage;
    $scope.deleteMessage = deleteMessage;

    function addMessage(){
        $scope.messages.$add({'username':$scope.username,'text':$scope.newMessage});
        $scope.newMessage = '';//borramos el input
    }

    function deleteMessage(index){

        $scope.messages.$remove($scope.messages[index]);
    }
}